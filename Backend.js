import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBqx3bXkeHKJKtZ6j6IEN29HLv0A5bfecM",
  authDomain: "presentapp-c137.firebaseapp.com",
  databaseURL: "https://presentapp-c137.firebaseio.com",
  projectId: "presentapp-c137",
  storageBucket: "presentapp-c137.appspot.com",
  messagingSenderId: "975400792544"
};

firebase.initializeApp(firebaseConfig);

const database = firebase.database();

export default database;