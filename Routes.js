import React, {Component} from 'react';
import {Router, Scene} from 'react-native-router-flux';
import Home from './components/Home';
import Login from './components/Login';
import Meditation from './components/Meditation';
import Journal from './components/Journal';
import Reading from './components/Reading';
import News from './components/News';
import Exercise from './components/Exercise';
import Nutrition from './components/Nutrition';

const Routes = () => (
  <Router>
    <Scene key="root">
      <Scene key="login" component={Login} title="Login" />
      <Scene key="home" component={Home} title="Home" initial hideNavBar />
      <Scene key="meditation" component={Meditation} title="Meditation" hideNavBar />
      <Scene key="journal" component={Journal} title="Journal" hideNavBar />
      <Scene key="reading" component={Reading} title="Reading" hideNavBar />
      <Scene key="news" component={News} title="News" hideNavBar />
      <Scene key="exercise" component={Exercise} title="Exercise" hideNavBar />
      <Scene key="nutrition" component={Nutrition} title="Nutrition" hideNavBar />
    </Scene>
  </Router>
);

export default Routes