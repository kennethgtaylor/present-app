import React from 'react';
import  { Actions } from 'react-native-router-flux';
import { View, StyleSheet, Text, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#F5FCFF',
    flex: 1
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  tile: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  tileContent: {
    fontSize: 20,
    color: '#fff',
    paddingRight: 20,
    paddingBottom: 20
  },
  meditation: {
    backgroundColor: '#551a8b'
  },
  journal: {
    backgroundColor: '#4c177d'
  },
  reading: {
    backgroundColor: '#44146f'
  },
  news: {
    backgroundColor: '#3b1261'
  },
  exercise: {
    backgroundColor: '#330f53'
  },
  nutrition: {
    backgroundColor: '#2a0d45'
  },
  personal: {
    backgroundColor: '#220a37'
  },
  financial: {
    backgroundColor: '#190729'
  },
  giving: {
    backgroundColor: '#11051b'
  }
});

// TODO: Get options from dashboard => 'Enabled Modules'?
const Home = () => (
      <View style={styles.container}>
        <TouchableOpacity style={[styles.tile, styles.meditation]}  onPress={() => Actions.push('meditation')}>
          <Text style={styles.tileContent}>Meditation</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tile, styles.journal]}  onPress={() => Actions.push('journal')}>
          <Text style={styles.tileContent}>Journal</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tile, styles.reading]}
        onPress={() => Actions.push('reading')}>
        <Text style={styles.tileContent}>Reading</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tile, styles.news]}
          onPress={() => Actions.push('news')}>
          <Text style={styles.tileContent}>News</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tile, styles.exercise]}
        onPress={() => Actions.push('exercise')}>
          <Text style={styles.tileContent}>Exercise</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tile, styles.nutrition]}
        onPress={() => Actions.push('nutrition')}>
          <Text style={styles.tileContent}>Nutrition</Text>
        </TouchableOpacity>
        {/* 
        <TouchableOpacity style={[styles.tile, styles.financial]}
        onPress={() => Actions.push('financial')}>
          <Text style={styles.tileContent}>Financial</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.tile, styles.giving]}
        onPress={() => Actions.push('giving')}>
          <Text style={styles.tileContent}>Giving Back</Text>
        </TouchableOpacity>
         */}
      </View>
    )

export default Home

