import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Box from './Box';

class Tracker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showTracker: false,
    }

    this.calculateDaysForCalendar = this.calculateDaysForCalendar.bind(this);
  }

  calculateDaysForCalendar() {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth();
    const daysInMonth = new Date(year, month + 1, 0).getDate();

    return daysInMonth;
  }

  render() {
    const num = this.calculateDaysForCalendar();
    const tracker = this.state.showTracker ? `Hide`: `Show`;
    let boxes = [];

    for (let i = 1; i <= num; i++) {
      boxes.push(<Box key={i} dayNum={i} />);
    }

    boxes = (this.state.showTracker) ? boxes : null

    return (
      <View style={{
          flexDirection: 'row', 
          justifyContent: 'center',
          flexWrap: 'wrap',
          alignItems: 'center',
          marginBottom: 10,
          marginTop: 15
        }}>
        {boxes}
        <Text style={{ textAlign: 'center', fontSize: 25, color: '#fff', fontWeight: 'bold', flexBasis: '100%', marginTop: 10 }} onPress={() => {
          this.setState({
            showTracker: !this.state.showTracker
          });
        }}>{tracker}</Text>
      </View>
    )
  }
}

export default Tracker