import React, {Component} from 'react';
import {Keyboard, Text, TextInput, TouchableOpacity, View, StyleSheet} from 'react-native';
import database from '../Backend'

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#330f53',
    paddingLeft: 5,
    paddingTop: 50,
    paddingBottom: 20,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  inputContainer: {
    flex: 1,
    width: 350,
    paddingLeft: 10,
    paddingTop: 10,
    paddingRight: 0,
    marginLeft: 0,
  },
  input: {
    flex: 1,
    fontSize: 18,
    color: '#fff',
    borderBottomWidth: 2,
    borderBottomColor: '#472664'
  },
  quoteContainer: {
    alignItems: 'center'
  },
  quoteText: {
    fontSize: 20,
    color: '#7e679280',
    margin: 10,
    fontStyle: 'italic',
    textAlign: 'justify',
  },
  big: {
    fontSize: 60,
    fontStyle: 'normal',
    fontWeight: 'bold',
    color: '#47266448',
  },
  wordCount: {
    textAlign: 'right',
    paddingTop: 10,
    color: '#fafafa48',
  },
});

class Journal extends Component {
  constructor() {
    super();

    this.state = {
      text: '',
      keyboardShowing: false,
      wordCount: 0,
      quote: ''
    }
  }
  componentWillMount() {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    
    database.ref(`${year}/${month}/${day}/journal`).once('value').then(( snapshot) => {
      if (snapshot.val() === null) return true;
      if (this.state.text) {
        this.wordCount(this.state.text);
      }
      return this.setState({text: snapshot.val()});
    }, error => {
      console.error(error);
    });
  }
  componentDidMount() {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    // const time  = date.toLocaleTimeString();

    database.ref(`${year}/${month}/${day}/journal`).once('value').then(snapshot => {;
      if (snapshot.val()) {
        this.setState({text: snapshot.val().text});
      }
    }, error => {
      console.error(error);
    });
    this.getQuote();
  }
  componentWillUnmount() {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    // const time  = date.toLocaleTimeString();

    return database.ref(`${year}/${month}/${day}/`).update({
      journal: this.state.text,
    });
  }
  async getQuote() {
    try {
      const responseRaw = await fetch('https://quotes.rest/qod.json?category=life');
      const response = await responseRaw.json();
      const quote = response.contents.quotes[0].quote;
      this.setState({quote});
    } catch (e) {
      console.error(e);
    }
  }
  wordCount(words) {
    // TODO: Display correct word count on re-entry.
    const splitTextAmount = words.text.split(' ').length - 1;

    this.setState({ text: words.text, wordCount: splitTextAmount });
  }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity activeOpacity={100} onPress={Keyboard.dismiss} >
          <View style={styles.inputContainer}>
            <TextInput 
              placeholder={'Am I Focused?'}
              placeholderTextColor={'#fff'}
              keyboardType={'default'}
              maxHeight={200}
              style={styles.input}
              autoGrow
              numberOfLines={7}
              keyboardAppearance={'dark'}
              multiline
              value={this.state.text}
              onChangeText={text => this.wordCount({text})}
            />
            <Text style={styles.wordCount} >{this.state.wordCount}/300</Text>
            </View>
            <View style={styles.quoteContainer}>
              <Text style={[styles.quoteText, styles.big]}>BE PRESENT</Text>
              <Text style={styles.quoteText}>"{this.state.quote}"</Text>
            </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default Journal

