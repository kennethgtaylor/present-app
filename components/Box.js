import React, {Component} from 'react';
import PropTypes from "prop-types";
import {View, Text, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  box: {
    backgroundColor: '#fafafa',
    flexBasis: '11%',
    height: 40,
    margin: 4,
    justifyContent: 'center',
    borderRadius: 10,
  },
  complete: {
    backgroundColor: '#90EE90',
  },
  text: {
    textAlign: 'center',
    color: '#1a1a1a',
    fontSize: 16,
  }

});

class Box extends Component {
  constructor(props) {
    super(props);

    this.state = {
      complete: false
    }
  }

  render() {
    return (
      <View style={[styles.box, this.state.complete && styles.complete]}>
        <Text style={styles.text} onPress={() => {
          if (this.state.complete) {
            this.setState({ complete: false })
          } else {
            this.setState({ complete: true })
          }
        }} complete={this.state.complete}>
          {this.props.dayNum}
        </Text>
      </View>
    )
  }
}

Box.propTypes = {
  dayNum: PropTypes.number
}

Box.defaultProps = {
  dayNum: 0
}

export default Box