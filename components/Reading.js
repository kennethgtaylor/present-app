import React, { Component } from 'react';
import {Keyboard, KeyboardAvoidingView, TextInput, TouchableOpacity, StyleSheet} from 'react-native';
import database from '../Backend'

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#330f53',
    paddingLeft: 5,
    paddingBottom: 20,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    flex: 1,
  },
  input: {
    flex: 1,
    fontSize: 20,
    marginTop: 25,
    marginBottom: 5,
    marginLeft: 15,
    marginRight: 15,
    borderRadius: 5,
    color: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
  }
});

class Reading extends Component {
  constructor() {
    super()

    this.state = {
      source: '',
      thoughts: ''
    }

    this.addSource = this.addSource.bind(this);
    this.addThoughts = this.addThoughts.bind(this);
    this.setData = this.setData.bind(this);
    this.getData = this.getData.bind(this);
  }
  componentWillMount() {
    this.getData();
  }
  componentWillUnmount() {
    this.setData(this.state.source, this.state.thoughts);
  }
  getData() {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    let data = database.ref(`${year}/${month}/${day}/reading/miscellaneous`).orderByKey().limitToFirst(1);

    console.log(data);
  }
  setData(source, thoughts) {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return database.ref(`${year}/${month}/${day}/reading/miscellaneous/`).push({
      source,
      thoughts
    });
  }
  addSource(source) {
    this.setState({source});
  }
  addThoughts(thoughts) {
    this.setState({thoughts});
  }
  render() {
    return (
      <TouchableOpacity activeOpacity={100} onPress={Keyboard.dismiss} style={styles.container}>
        <KeyboardAvoidingView behavior='padding' style={styles.container} enabled>
          <TextInput
            style={styles.input}
            editable
            placeholderTextColor={'#fff'}
            allowFontScaling
            multiline
            onChangeText={(source) => this.addSource(source)}
            placeholder={'What did you read today?'}
            value={this.state.source} />
          <TextInput
            style={styles.input}
            editable
            placeholderTextColor={'#fff'}
            allowFontScaling
            multiline
            value={this.state.thoughts}
            onChangeText={(thoughts) => this.addThoughts(thoughts)}
            placeholder={'What thoughts struck out most while reading this?'} />
        </KeyboardAvoidingView>
      </TouchableOpacity>
    )
  }
}

export default Reading

