import React from 'react'
import { Button, View, StyleSheet, Text, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  textInput: {
    textAlign: 'center',
    color: '#1a1a1a',
    marginBottom: 10,
    borderBottomWidth: 1,
    width: 200,
    height: 30
  },
});

const Login = () => (
  <View style={styles.container}>
    <Text style={styles.welcome}>
      _YourTruths
        </Text>
    <TextInput style={styles.textInput} placeholder="Username" />
    <TextInput style={styles.textInput} placeholder="Email" />
    <Button
      onPress={() => Actions.push('home')}
      title="Sign In"
      color="#841584"
    />
  </View>
)

export default Login

