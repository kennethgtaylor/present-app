import React, {Component} from 'react';
import { 
  StyleSheet, 
  Text, 
  TouchableOpacity, 
  View
} from 'react-native';
import Sound from 'react-native-sound';
import database from '../Backend';

// TODO: UTILIZE http://momentjs.com/ FOR TIME
// TODO: PUSH NOTIFICATION FOR WHEN PHONE SLEEPS

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#330f53',
    paddingLeft: 5,
    paddingTop: 25,
    paddingBottom: 20,
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  bigNums: {
    fontSize: 70,
    color: '#fff'
  },
  controlBtns: {
    flexDirection: 'row',
  },
  metaContainer: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  meta: {
    width: 200,
    marginTop: 10,
    marginBottom: 45,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  presetMinutes: {
    fontSize: 20,
    color: '#fff'
  },
  btn: {
    paddingTop: 13,
    paddingBottom: 13,
    paddingLeft: 35,
    paddingRight: 35,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 5,
  },
  btnTxt: {
    fontSize: 18,
    color: '#fff',
  },
  start: {
    backgroundColor: '#472664'
  },
  end: {
    backgroundColor: '#311a46'
  },
  active: {
    backgroundColor: '#a392b1',
  },
  sounds: {
    alignItems: 'flex-end',
  },
  quoteContainer: {
    alignItems: 'flex-end'
  },
  quoteText: {
    fontSize: 20,
    color: '#7e679280',
    margin: 10,
    fontStyle: 'italic',
    textAlign: 'justify',
  },
  big: {
    fontSize: 60,
    fontStyle: 'normal',
    fontWeight: 'bold',
    color: '#47266448',
  },
});

Sound.setCategory('Playback');

const startSound = new Sound(
  'Winking Face.wav', 
  Sound.MAIN_BUNDLE,
  (error) => {
    if (error) {
      console.log('failed to load startSound', error);
      return;
    }
    // Sound loaded
    console.log('startSound loaded');
});
  
const endSound = new Sound(
  'Unamused Face.wav', 
  Sound.MAIN_BUNDLE, 
  (error) => {
    if (error) {
      console.log('failed to load endSound', error);
      return;
    }
    // Sound successfully loaded
    console.log('endSound loaded successfully')
});

class Meditation extends Component {
  constructor() {
    super();

    this.state = {
      hours: 0,
      minutes: 0,
      seconds: 0,
      minutesSet: false,
      habitStreaK: 0,
      meditated: false,
      quote: '',
    }

    this.tickUp = this.tickUp.bind(this);
    this.tickDown = this.tickDown.bind(this);
    this.setMinutes = this.setMinutes.bind(this);
    this.clearTick = this.clearTick.bind(this);
    this.startTick = this.startTick.bind(this);
    this.formatNumbers = this.formatNumbers.bind(this);
    this.updateMeditation = this.updateMeditation.bind(this);
    this.getQuote = this.getQuote.bind(this);
  }

  componentDidMount() {
    const { seconds, minutes, hours } = this.state;
    this.clearTick()
    this.formatNumbers(seconds, minutes, hours);
    this.endBtn.setOpacityTo(0, 0);
    this.getQuote();
  }
  setMinutes(selectedMinutes) {
    clearInterval(this.tickDownId)
    clearInterval(this.tickUpId)
    
    if (selectedMinutes !== 50) {
      this.updateMeditation(selectedMinutes);
      this.setState({ meditated: true });
    }
    const seconds = `00`.toString()
    let minutes = selectedMinutes;
    if (selectedMinutes < 10) {
      minutes = (`0${selectedMinutes}`).toString()
    }

    this.setState({minutes, seconds, minutesSet: true})
  }
  async getQuote() {
    try {
      const responseRaw = await fetch('https://quotes.rest/qod.json?category=inspire');
      const response = await responseRaw.json();
      const quote = response.contents.quotes[0].quote;
      this.setState({quote});
    } catch (error) {
      console.error(error);
    }
  }
  updateMeditation(selectedMinutes) {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    const meditation = database.ref(`${year}/${month}/${day}/meditation`);
    meditation.set({
      duration: selectedMinutes
    });
  }
  tickDown() {
    this.tickDownId = setInterval(() => {
      const hours   = this.state.hours;
      let seconds = this.state.seconds;
      let minutes = this.state.minutes;

      if (this.state.seconds === '00') {
        seconds = 59
        minutes--
      } else if (this.state.seconds !== 0) {
        seconds--
      } else if(this.state.minutes !== 0) {
        seconds = 59
        minutes--
      } else {
        minutes--
        seconds = 59
      }
      
      this.formatNumbers(seconds, minutes, hours);
    }, 1000);
  }
  tickUp() {
    this.tickUpId = setInterval(() => {
      let seconds = this.state.seconds;
      let minutes = this.state.minutes;
      let hours = this.state.hours;

      if (this.state.seconds !== 59) {
        seconds++
      } else if (this.state.minutes !== 59) {
        seconds = 0;
        minutes++;
      } else {
        minutes = 0;
        hours++;
      }

      this.formatNumbers(seconds, minutes, hours);
    }, 1000);
  }
  clearTick() {
    clearInterval(this.tickDownId)
    clearInterval(this.tickUpId)

    const seconds = ('00').toString()
    const minutes = ('00').toString()
    const hour = ('00').toString()

    this.setState({seconds, minutes, hour, minutesSet: false});
  }
  startTick() {
    if (this.state.minutesSet) {
      this.tickDown()
    } else {
      this.clearTick()
      this.tickUp()
    }

    this.startBtn.setOpacityTo(0, 250);
    this.endBtn.setOpacityTo(1, 250);

    startSound.setCurrentTime(0);
    startSound.play();
  }
  formatNumbers(sec, min, hr) {
    let formattedSeconds = sec;
    let formattedMinutes = min;
    let formattedHours = hr;

    if (formattedSeconds < 10) {
      formattedSeconds = (`0${sec}`).slice(-2);
    }
    if (formattedMinutes < 10) {
      formattedMinutes = (`0${min}`).slice(-2);
    }
    if (formattedHours < 10) {
      formattedHours = (`0${hr}`).slice(-2);
    }

    if (formattedSeconds === '00' && formattedMinutes === '00' && formattedHours === '00' && this.state.meditated) {
      startSound.setCurrentTime(0);
      startSound.play();
      this.clearTick();
    }
    
    this.setState({
      seconds: formattedSeconds,
      minutes: formattedMinutes,
      hours: formattedHours
    });
  }
  render() {
    const { hours, minutes, seconds } = this.state;
    return (
      <View style={styles.container}>
        {/* <Tracker /> */}
        <View style={styles.metaContainer}>
          <Text style={styles.bigNums}>{hours}:{minutes}:{seconds}</Text>
          <View style={styles.meta}>
            <Text onPress={() => {this.setMinutes(5)}} style={styles.presetMinutes}>5</Text>
            <Text onPress={() => {this.setMinutes(10)}} style={styles.presetMinutes}>10</Text>
            <Text onPress={() => {this.setMinutes(15)}} style={styles.presetMinutes}>15</Text>
            <Text onPress={() => {this.setMinutes(30)}} style={styles.presetMinutes}>30</Text>
            <Text onPress={() => {this.setMinutes(50)}} style={styles.presetMinutes}>50</Text>
          </View>
          <View style={styles.controlBtns}>
            <TouchableOpacity 
              style={[styles.end, styles.btn]} 
              underlayColor={'#330f53'}
              ref={ c => this.endBtn = c }
              onPress={() => {
                this.clearTick();
                this.endBtn.setOpacityTo(0, 250);
                this.startBtn.setOpacityTo(1, 250);
                startSound.setCurrentTime(0);
                startSound.play((success) => {
                  if (success) console.log('endSound Played');
                  else {
                    console.log('Failed');
                    startSound.reset();
                  }
                });
              }}
            >
              <Text style={styles.btnTxt}>End</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.start, styles.btn]} 
              ref={ c1 => this.startBtn = c1 }
              onPress={() => {this.startTick()}}
            >
              <Text style={styles.btnTxt}>Start</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.quoteContainer}>
            <Text style={[styles.quoteText, styles.big]}>BE PRESENT</Text>
            <Text style={styles.quoteText}>"{this.state.quote}"</Text>
          </View>
        </View>
        {/* <View style={styles.sounds}>
          <Text style={{color: '#fff'}}>Rain Fire White noise Coffee Nightime</Text>
            </View> */}
      </View>
    )
  }
}

export default Meditation