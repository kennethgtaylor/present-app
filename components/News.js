/* eslint react/no-multi-comp: 0 */
/* eslint no-console: 0 */

import React, {Component} from 'react'
import PropTypes from "prop-types";
import {ImageBackground, Linking, View, Text, TouchableOpacity, ScrollView, StyleSheet} from 'react-native'
import database from '../Backend'

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#330f53',
    flex: 1,
  },
  content: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'right',
    paddingLeft: 50,
    paddingRight: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    alignItems: 'flex-end'
  },
  img: {
    height: 200,
  },
  contentBg: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    flex: 1,
    justifyContent: 'flex-end',
  }
});

class Article extends Component {
  constructor(props) {
    super(props);

    this.goToArticle = this.goToArticle.bind(this);
  }
  goToArticle(url, title) {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    // TODO: Correct duplication in DB. 
    // TODO: Check if articles is already present
    database.ref(`${year}/${month}/${day}/reading/news/`).push({
      url,
      title
    });
    
    Linking.openURL(url)
      .catch(err => {
        console.error(err)
      });
  }
  render() {
    return (
      <ImageBackground
          style={styles.img}
          source={{uri: this.props.imgUri }}>
        <View style={styles.contentBg}>
          <TouchableOpacity onPress={() => this.goToArticle(this.props.url, this.props.title)}>
            <Text style={styles.content}>{this.props.title}</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
}

class News extends Component {
  constructor() {
    super();

    this.url = 'https://newsapi.org/v2/top-headlines?country=us&apiKey=0f5ca0b2909f49159cd53f8bcd9c809e';

    this.state = {
      articles: []
    }

    this.getNews = this.getNews.bind(this);
  }
  componentWillMount() {
    this.getNews()
  }
  async getNews() {
    try {
      const rawResponse = await fetch(this.url);
      const response = await rawResponse.json();
      const articleList = await response.articles;
      const articles = [];

      // TODO: Figure out why some images don't display
      articleList.forEach((article, i) => {
        console.log(article.urlToImage);
        if (article.urlToImage) {
          articles.push(
            <Article
              key={i}
              title={article.title ? article.title : ''}
              imgUri={(article.urlToImage === null) ? 'https://picsum.photos/1920/800/?random' : article.urlToImage}
              url={article.url ? article.url : ''} />
          );
        } else {
          articles.push(
            <Article
              key={i}
              title={article.title ? article.title : ''}
              imgUri={'https://picsum.photos/1920/800/?random'}
              url={article.url ? article.url : ''} />
          );
        }
      });
      this.setState({ articles });
    } catch (e) {
      if (e) console.error(e);
    }
  }
  render() {
    const articles = []

    for (let i = 0; i < this.state.articles.length; i++) {
      articles.push(this.state.articles[i]);
    }
    return (
      <ScrollView style={styles.container}>
        {articles}
      </ScrollView>
    )
  }
}

Article.defaultProps = {
  imgUri: 'https://picsum.photos/200/300/?random',
  url: '',
  title: '',
}

Article.propTypes = {
  imgUri: PropTypes.string,
  url: PropTypes.string,
  title: PropTypes.string
}
export default News

