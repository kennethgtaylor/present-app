import React, {Component} from 'react';
import {Modal, View, StyleSheet, TextInput, Text, TouchableOpacity} from 'react-native';
import database from '../Backend';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#330f53',
    paddingTop: 30,
    justifyContent: 'space-between',
  },
  btns: {
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 15,
    margin: 10,
    flex: 1,
    backgroundColor: '#e9e9e9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  darkBtns: {
    margin: 10,
    color: '#fff',
    backgroundColor: '#330f53',
  },
  drkContent: {
    color: '#191919',
    fontWeight: 'bold',
    fontSize: 15
  },
  content: {
    color: '#fff',
    fontSize: 20
  },
  btnContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    alignSelf: 'flex-end',
  },
  itemInput: {
    flex: 3,
    height: 40,
    flexDirection: 'row',
    margin: 5,
    color: '#fff',
    backgroundColor: '#330f53',
    borderBottomColor: '#fff',
    borderBottomWidth: 2,
  },
  small: {
    flex: 1,
  },
  exerciseItem: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
  },
  workoutForm: {
    flex: 1
  },
  modalContainer: {
    marginTop: 40,
    paddingTop: 40,
    marginLeft: 10,
    marginRight: 10,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#330f53'
  },
  modalContent: { 
    flexGrow: 1,
    marginTop: 50, 
    marginLeft: 10,
  },
  modalInputs: {
    borderBottomColor: '#330f53',
    color: '#000',
    borderBottomWidth: 1,
    margin: 10,
    flexGrow: 1,
  },
  modalBtn: {
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 15,
    margin: 10,
    backgroundColor: '#e9e9e9',
    alignItems: 'center',
    justifyContent: 'center'
  },
  center: {
    textAlign: 'center',
  }
});

const ExerciseItem = (props) => (
    <View style={styles.exerciseItem}>
      <Text style={styles.itemInput}>{props.name}</Text>
      <Text style={[styles.itemInput, styles.small]}>{props.sets}</Text>
      <Text style={[styles.itemInput, styles.small]}>{props.reps}</Text>
      <Text style={styles.itemInput}>{props.weight}</Text>
    </View>
  );

class Exercise extends Component {
  constructor() {
    super();

    this.state = {
      showCustomForm: false,
      showWeightsForm: !this.showCustomForm,
      modalVisible: false,
      rows: [],
      counter: 0,
    }

    this.addWorkoutRow = this.addWorkoutRow.bind(this);
    this.makeId = this.makeId.bind(this);
  }
  componentWillMount() {
    const date = new Date();
    const year = date.getFullYear()
    const month = date.getMonth() + 1;
    const day = date.getDate();

    database.ref(`${year}/${month}/${day}/workout`).once('value').then(snapshot => {
      if (snapshot.val() === null) return true;
      return this.setState({ rows: snapshot.val() });
    }, error => {
      console.error(error);
    });
  }
  componentDidMount() {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    database.ref(`${year}/${month}/${day}/workout`).once('value').then(snapshot => {
      this.setState({ rows: snapshot.val() });
    }, error => {
      console.error(error);
    });
  }
  componentWillUpdate() {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();

    return database.ref(`${year}/${month}/${day}/`).update({
      workout: this.state.rows,
    });
  }
  componentWillUnmount() {
    const date = new Date();
    const year = date.getFullYear()
    const month = date.getMonth() + 1;
    const day = date.getDate();

    return database.ref(`${year}/${month}/${day}/`).update({
      workout: this.state.rows,
    });
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible})
  }
  makeId() {
    let id = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
      id += possible.charAt(Math.floor(Math.random() * possible.length));

    return id;
  }
  addWorkoutRow() {
    const name = this.name._lastNativeText;
    const sets = this.sets._lastNativeText;
    const reps = this.reps._lastNativeText;
    const weight = this.weight._lastNativeText;
    const id = this.makeId();

    const newRow = { id, name, sets, reps, weight };
    const rows = this.state.rows || [];

    rows.push(newRow);

    this.setState({ rows });
    this.setModalVisible(!this.state.modalVisible);
  }
  render() {
    const workoutInfo = this.state.rows;
    let rows = null;
    if (workoutInfo != null && workoutInfo.length > 0 ) {
      rows = workoutInfo.map((row) =>
        <ExerciseItem
          key={row.id}
          name={row.name}
          sets={row.sets}
          reps={row.reps}
          weight={row.weight}
        />
      );
    }
    console.log(this.state.rows);
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 20, color: '#fff', fontWeight: 'bold' }}>Workout</Text>
        <Modal
          style={styles.modalContainer}
          animationType="fade"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => console.log('closed')}
          presentationStyle="overFullScreen"
        >
          <TextInput
            ref={(input) => { this.name = input; }} 
            style={styles.modalInputs}
            placeholderTextColor={'#fff'}
            placeholder={'Exercise Name'} />
          <TextInput
            ref={(input) => { this.sets = input; }} 
            style={styles.modalInputs}
            placeholderTextColor={'#fff'}
            placeholder={'Sets'} />
          <TextInput
            ref={(input) => { this.reps = input; }} 
            style={styles.modalInputs}
            placeholderTextColor={'#fff'}
            placeholder={'Reps'} />
          <TextInput
            ref={(input) => { this.weight = input; }} 
            style={styles.modalInputs}
            placeholderTextColor={'#fff'}
            placeholder={'Weight'} />
          <TouchableOpacity
            onPress={() => this.addWorkoutRow()}
            style={styles.modalBtn}>
            <Text style={[styles.drkContent, styles.center]}>+</Text>
          </TouchableOpacity>
        </Modal>
        {rows}
        <View style={styles.btnContainer}>
          <TouchableOpacity 
            onPress={() => this.setModalVisible(!this.state.modalVisible)} 
            style={styles.btns}>
            <Text style={styles.drkContent}>Add Workout</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default Exercise